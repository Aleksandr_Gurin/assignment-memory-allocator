#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>


int main(){
    // Test 1

    heap_init(5000);

    void *mem = _malloc(50);

    debug_heap(stdout, HEAP_START);

    //Test2
    void *mem2 = _malloc(100);
    void *mem3 = _malloc(200);
    _free(mem2);
    debug_heap(stdout, HEAP_START);

    //Test3
    _free(mem);
    debug_heap(stdout, HEAP_START);

    //Test4
    void *mem4 = _malloc(5000);
    debug_heap(stdout, HEAP_START);

    _free(mem4);
    _free(mem3);
}